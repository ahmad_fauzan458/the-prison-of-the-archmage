extends Node2D

onready var dialog_text = get_node("/root/Stage/CanvasLayer/DialogBox/RichTextLabel")
onready var lives_label = get_node("/root/Stage/CanvasLayer/LivesLabel")
onready var orb_count_label = get_node("/root/Stage/CanvasLayer/OrbCountLabel")

func _ready():
	Global.dark_exposure_time = 0
	Global.dark_exposure_time_limit = 4;
	Global.exposed_to_dark = false;
	Global.on_cooldown_light_of = false;
	Global.time_until_recover = 0
	Global.lives = 5
	Global.max_lives = 5
	Global.die = false
	Global.stage_start = true
	Global.teleport_marker_instance = null
	Global.current_orb = 0
	Global.orbs_limit = 5
	Global.timer_on_stop = false

	lives_label.text = 'Lives: ' + str(Global.lives) + '/' + str(Global.max_lives)
	orb_count_label.text = 'Orb Collected: ' + str(Global.current_orb) + '/' + str(Global.orbs_limit)

	if Global.stage_start:
		Global.stage_start = false
		dialog_text.run_dialog([
			'Alastair:\nHaahh ...',
			'Alastair:\nSegel Aldeina, aku tidak sangka mereka menggunakan hal itu.',
			'Alastair:\nHampir semua kekuatanku telah tersegel dan aku terperangkap di tempat ini',
			'Alastair:\nTempat ini sama seperti di catatan yang ku miliki.',
			'Alastair:\nKegelapan mengerikan yang akan membunuhku apabila aku terlalu lama di dalamnya.',
			'Alastair:\nMonster yang sangat mematikan namun hanya menyerang dalam terang.',
			'Alastair:\nAku harus menunggu segel di orb-orb tersebut terbuka, dan segera ' +
						'mengumpulkan mereka apabila telah muncul di tempat ini.'
		], null, 'Alastair')


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
