extends ParallaxLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var dialog_text = get_node("/root/Node2D/DialogBox/RichTextLabel")

# Called when the node enters the scene tree for the first time.
func _ready():
	dialog_text.run_dialog([
		'Maret 1385 Kalendar Lumina, wilayah manusia hanya tersisa satu per ' +
		'empatnya. Para pahlawan hanya dapat menghambat invasi Alastair ' +
		'dan prajuritnya, tapi tidak dapat memukul mundur mereka.'],
		"res://Scenes/FourthStory.tscn")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
