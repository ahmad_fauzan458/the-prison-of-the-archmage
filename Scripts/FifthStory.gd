extends ParallaxLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var dialog_text = get_node("/root/Node2D/DialogBox/RichTextLabel")

# Called when the node enters the scene tree for the first time.
func _ready():
	dialog_text.run_dialog([
		'Mei 1385 Kalendar Lumina, para pahlawan berhasil menyegel Alastair. ' +
		'Kelima orbs yang digunakan untuk menyegel Alastair disimpan ' +
		'oleh masing-masing pahlawan.'], "res://Scenes/SixthStory.tscn")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
