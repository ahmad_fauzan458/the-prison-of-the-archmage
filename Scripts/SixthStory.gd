extends ParallaxLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var dialog_text = get_node("/root/Node2D/DialogBox/RichTextLabel")

# Called when the node enters the scene tree for the first time.
func _ready():
	dialog_text.run_dialog([
		'Alastair tidak dapat keluar ' +
		'dari tempat dia disegel apabila segel pada kelima orbs ' +
		'tersebut masih ada. Setiap kali segel pada sebuah orb dibuka ' + 
		'maka akan muncul sebuah orb di tempat Alastair di segel. ' +
		'Apabila Alastair berhasil mengumpulkan kelima orb yang akan muncul ' +
		'tersebut, maka Alastair dapat kembali ke dunianya.'],
		"res://Scenes/Stage.tscn")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
