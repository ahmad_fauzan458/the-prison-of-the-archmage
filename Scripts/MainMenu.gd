extends LinkButton


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Play_pressed():
	Global.stage_start = true
	get_tree().change_scene(str("res://Scenes/FirstStory.tscn"))


func _on_Quit_pressed():
	get_tree().quit()


func _on_Control_pressed():
	get_tree().change_scene(str("res://Scenes/Guide.tscn"))
