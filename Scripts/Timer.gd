extends Timer

onready var dark_exposure_time_label = get_node("/root/Stage/CanvasLayer/DarkExposureTimeLabel")
onready var time_until_recover_label = get_node("/root/Stage/CanvasLayer/TimeUntilRecoverLabel")
onready var large_light_cooldown_label = get_node("/root/Stage/CanvasLayer/LargeLightCooldownLabel")
onready var lives_label = get_node("/root/Stage/CanvasLayer/LivesLabel")
onready var timer = get_node("../Timer")
onready var light_larger_cooldown = get_node("../LightLargerCooldown")
onready var light_larger_timer = get_node("../LightLargerTimer")
onready var player = get_node("../../Player")

var darkness_time_out = false

func _on_Timer_timeout():
	timer.wait_time = 1
	if Global.exposed_to_dark:
		if darkness_time_out:
			darkness_time_out = false
			Global.die = true
			if (Global.lives <= 1 and Global.die):
				get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
			else:
				player.position = Global.player_pos
				Global.player_spawn()
				time_until_recover_label.text = "Time Until Recover: 0"
				dark_exposure_time_label.text = "Dark Exposure Time: 0/" + str(Global.dark_exposure_time_limit)
				timer.wait_time = 1
				return

		time_until_recover_label.text = "Time Until Recover: 0"
		Global.dark_exposure_time += 1
		dark_exposure_time_label.text = "Dark Exposure Time: " + str(Global.dark_exposure_time) + "/" + str(Global.dark_exposure_time_limit)
		if Global.dark_exposure_time < Global.dark_exposure_time_limit:
			timer.wait_time = 1
			timer.start()
		else:
			darkness_time_out = true;
			timer.wait_time = 0.2
			timer.start()

	else:
		darkness_time_out = false
		dark_exposure_time_label.text = "Dark Exposure Time: 0/" + str(Global.dark_exposure_time_limit)

		Global.time_until_recover -= 1

		time_until_recover_label.text = "Time Until Recover: " + str(Global.time_until_recover)
		if Global.time_until_recover > 0:
			timer.start()
		else:
			Global.on_cooldown_light_of = false;
			timer.stop()


func _on_LightLargerCooldown_timeout():
	light_larger_timer.stop()
	Global.large_light_cooldown -= 1;
	large_light_cooldown_label.text = "Large Light Cooldown: " + str(Global.large_light_cooldown)
	
	if Global.large_light_cooldown > 0 :
		light_larger_cooldown.start()
	else:
		Global.large_light_cooldown = 0
		light_larger_cooldown.stop()
		
