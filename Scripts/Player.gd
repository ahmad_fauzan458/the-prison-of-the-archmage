extends KinematicBody2D

export (int) var speed = 400
export (int) var gravity = 1200
export (int) var jump_speed = -500
export (int) var bounce_back = 10000
export (int) var jump_limit = 1

const UP = Vector2(0,-1)

var velocity = Vector2()
var direction = Vector2(0, -1)
var current_jump = 0
onready var light = get_node("Light")
onready var light_larger = get_node("LightLarger")
onready var dark = get_node("Dark")
onready var timer = get_node("Timer")
onready var light_larger_timer = get_node("LightLargerTimer")
onready var light_larger_cooldown = get_node("LightLargerCooldown")
onready var teleport_timer = get_node("TeleportTimer")
onready var dark_exposure_time_label = get_node("/root/Stage/CanvasLayer/DarkExposureTimeLabel")
onready var time_until_recover_label = get_node("/root/Stage/CanvasLayer/TimeUntilRecoverLabel")
onready var light_larger_cooldown_label = get_node("/root/Stage/CanvasLayer/LargeLightCooldownLabel")
onready var orb_count_label = get_node("/root/Stage/CanvasLayer/OrbCountLabel")
onready var nearest_orb_distance = get_node("/root/Stage/CanvasLayer/NearestOrbDistanceLabel")
onready var dialog_box = get_node("/root/Stage/CanvasLayer/DialogBox")
onready var dialog_text = get_node("/root/Stage/CanvasLayer/DialogBox/RichTextLabel")
onready var player = get_node("../Player")
onready var player_animation = get_node("../Player/AnimatedSprite")
onready var orb_1 = get_node("../Orb/AnimatedSprite")
onready var orb_2 = get_node("../Orb2/AnimatedSprite")
onready var orb_3 = get_node("../Orb3/AnimatedSprite")
onready var orb_4 = get_node("../Orb4/AnimatedSprite")
onready var orb_5 = get_node("../Orb5/AnimatedSprite")
const marker = preload("res://Scenes/Marker.tscn")
const teleport_marker = preload("res://Scenes/TeleportMarker.tscn")

func _ready():
	Global.player_pos = get_node("../Player").position

func get_input():	
	velocity.x = 0

	var up = Input.is_action_just_pressed('up')
	var left = Input.is_action_pressed('left')
	var right = Input.is_action_pressed('right')
	var toggle_light = Input.is_action_just_pressed('toggle_light')
	var toggle_light_larger = Input.is_action_just_pressed('toggle_light_larger')
	var mark = Input.is_action_just_pressed('mark')
	var teleport_mark = Input.is_action_just_pressed('teleport_mark')
	
	if true:
		var distance_to_orb_1 = 2147483647
		if (orb_1):
			distance_to_orb_1 = sqrt(pow((player_animation.global_position.x - orb_1.global_position.x),2) + pow((player_animation.global_position.y - orb_1.global_position.y),2))
		
		var distance_to_orb_2 = 2147483647
		if (orb_2):
			distance_to_orb_2 = sqrt(pow((player_animation.global_position.x - orb_2.global_position.x),2) + pow((player_animation.global_position.y - orb_2.global_position.y),2))
		
		var distance_to_orb_3 = 2147483647
		if (orb_3):
			distance_to_orb_3 = sqrt(pow((player_animation.global_position.x - orb_3.global_position.x),2) + pow((player_animation.global_position.y - orb_3.global_position.y),2))
		
		var distance_to_orb_4 = 2147483647
		if (orb_4):
			distance_to_orb_4 = sqrt(pow((player_animation.global_position.x - orb_4.global_position.x),2) + pow((player_animation.global_position.y - orb_4.global_position.y),2))
		
		var distance_to_orb_5 = 2147483647
		if (orb_5):
			distance_to_orb_5 = sqrt(pow((player_animation.global_position.x - orb_5.global_position.x),2) + pow((player_animation.global_position.y - orb_5.global_position.y),2))
		
		var nearest_orb = distance_to_orb_1
		var nearest_orb_text = "(Orb Z)"
		if (distance_to_orb_2 < nearest_orb):
			nearest_orb = distance_to_orb_2
			nearest_orb_text = "(Orb X)"
		if (distance_to_orb_3 < nearest_orb):	
			nearest_orb = distance_to_orb_3
			nearest_orb_text = "(Orb C)"
		if (distance_to_orb_4 < nearest_orb):
			nearest_orb = distance_to_orb_4
			nearest_orb_text = "(Orb V)"
		if (distance_to_orb_5 < nearest_orb):
			nearest_orb = distance_to_orb_5
			nearest_orb_text = "(Orb B)"
		
		nearest_orb_distance.text = 'Nearest Orb Distance: ' + str(nearest_orb) + ' ' + nearest_orb_text;
		
	if is_on_floor() and mark:
		var marker_instance = marker.instance()
		marker_instance.position = player.get_global_position()
		marker_instance.visible = true
		get_node("/root/Stage").add_child(marker_instance)

	if teleport_mark:
		if Global.teleport_marker_instance == null:
			Global.teleport_marker_instance = teleport_marker.instance()
			Global.teleport_marker_instance.get_child(0).play("Idle")
			Global.teleport_marker_instance.position = player.get_global_position()
			Global.teleport_marker_instance.visible = true
			get_node("/root/Stage").add_child(Global.teleport_marker_instance)
		else:
			player.position = Global.teleport_marker_instance.get_global_position()
			Global.teleport_marker_instance.get_child(0).play("Destroy")
			teleport_timer.wait_time = 0.5
			teleport_timer.start()

	if (toggle_light_larger && light.enabled && Global.large_light_cooldown <= 0):
		light_larger.enabled = true;
		light_larger_timer.start();
			
	if toggle_light:
		if (!dark.enabled && Global.on_cooldown_light_of):
			return
			
		light.enabled = !light.enabled;
		dark.enabled = !dark.enabled;
		
		if (dark.enabled):
			Global.dark_exposure_time = 1
			Global.on_cooldown_light_of = true;
			dark_exposure_time_label.text = "Dark Exposure Time: " + str(Global.dark_exposure_time) + "/" + str(Global.dark_exposure_time_limit)
			Global.timer_on_stop = false
			timer.start();
			Global.exposed_to_dark = true;
		else:
			Global.time_until_recover = 2 * Global.dark_exposure_time
			Global.dark_exposure_time = 0
			dark_exposure_time_label.text = "Dark Exposure Time: " + str(Global.dark_exposure_time) + "/" + str(Global.dark_exposure_time_limit)
			time_until_recover_label.text = "Time Until Recover: " + str(Global.time_until_recover)
			Global.timer_on_stop = false
			timer.start();
			Global.exposed_to_dark = false;
	
	if !left and !right and is_on_floor():
		$AnimatedSprite.play("Idle")

	if !is_on_floor():
		$AnimatedSprite.play("Jump")

	if is_on_floor():
		current_jump = 0;
		
	if is_on_floor() and up:
		velocity.y += jump_speed

	if up and current_jump < jump_limit:
		velocity.y = 0
		velocity.y += jump_speed
		current_jump += 1;
		
	if right:
		$AnimatedSprite.flip_h = false;
		velocity.x += speed
		if is_on_floor():
			$AnimatedSprite.play("Walk")
	
	if left:
		$AnimatedSprite.flip_h = true
		velocity.x -= speed
		if is_on_floor():
			$AnimatedSprite.play("Walk")
		
func get_raycast_input():
	if get_node("RayCastLeft").is_colliding():
		var object = get_node("RayCastLeft").get_collider()
		if object.is_in_group("orbs") && Input.is_action_just_pressed('interact'):
			destroy_orb(object)
			run_dialog_box()
	
	if get_node("RayCastRight").is_colliding():
		var object = get_node("RayCastRight").get_collider()
		if object.is_in_group("orbs") && Input.is_action_just_pressed('interact'):
			destroy_orb(object)
			run_dialog_box()
			
	if get_node("RayCastUp").is_colliding():
		var object = get_node("RayCastUp").get_collider()
		if object.is_in_group("orbs") && Input.is_action_just_pressed('interact'):
			destroy_orb(object)
			run_dialog_box()
			
	if get_node("RayCastDown").is_colliding():
		var object = get_node("RayCastDown").get_collider()
		if object.is_in_group("orbs") && Input.is_action_just_pressed('interact'):
			destroy_orb(object)
			run_dialog_box()

func destroy_orb(orb):
	Global.current_orb += 1;
	orb_count_label.text = 'Orb Collected: ' + str(Global.current_orb) + '/' + str(Global.orbs_limit)
	orb.get_owner().queue_free()

func run_dialog_box():
	if (dark.enabled):
		light.enabled = true;
		dark.enabled = false;
		
		Global.time_until_recover = 2 * Global.dark_exposure_time
		Global.dark_exposure_time = 0
		dark_exposure_time_label.text = "Dark Exposure Time: " + str(Global.dark_exposure_time) + "/" + str(Global.dark_exposure_time_limit)
		time_until_recover_label.text = "Time Until Recover: " + str(Global.time_until_recover)
		Global.timer_on_stop = false
		timer.start();
		Global.exposed_to_dark = false;

	if Global.current_orb >= Global.orbs_limit:
		dialog_text.run_dialog(
			Global.orbs_message[Global.current_orb-1],
			str("res://Scenes/Win.tscn"),
			"Message on the orb"
		)
	else:
		dialog_text.run_dialog(
			Global.orbs_message[Global.current_orb-1],
			null,
			"Message on the orb"
		)

func _physics_process(delta):
	if (Global.dialog_on):
		$AnimatedSprite.play("Idle")
	else:

		velocity.y += delta * gravity
		get_input()
		get_raycast_input()
		velocity = move_and_slide(velocity, UP)


func _on_LightLargerTimer_timeout():
	light_larger.enabled = false;
	Global.large_light_cooldown = 5;
	light_larger_cooldown_label.text = "Large Light Cooldown: " + str(Global.large_light_cooldown)
	light_larger_cooldown.start();
