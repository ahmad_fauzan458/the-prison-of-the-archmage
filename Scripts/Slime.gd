extends Area2D

func _on_Slime_body_entered(body):
	var current_scene = get_tree().get_current_scene().get_name()
	if body.get_name() == "Player" && !Global.exposed_to_dark:
		Global.die = true;
		if (Global.lives <= 1 and Global.die):
			get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
		else:
			body.position = Global.player_pos
			Global.player_spawn()

