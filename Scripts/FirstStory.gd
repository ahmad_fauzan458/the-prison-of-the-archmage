extends ParallaxLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var dialog_text = get_node("/root/Node2D/DialogBox/RichTextLabel")

# Called when the node enters the scene tree for the first time.
func _ready():
	dialog_text.run_dialog([
		'Februari 1384 Kalendar Lumina, Archmage Alastair menyatakan ' +
		'dirinya akan memimpin para demi-human dan monster untuk memusnahkan ' +
		'seluruh manusia.'], "res://Scenes/SecondStory.tscn")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
