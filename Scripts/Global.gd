extends Node

var dark_exposure_time = 0
var dark_exposure_time_limit = 4;
var exposed_to_dark = false;
var on_cooldown_light_of = false;
var dialog_on = false;
var time_until_recover = 0
var large_light_cooldown = 0
var lives = 5
var max_lives = 5
var die = false
var stage_start = true
var teleport_marker_instance = null
var current_orb = 0
var orbs_limit = 5
var player_pos = null
var orbs_message = [
	[
		'Message on the orb:\nAyah, ini aku Luze, aku menanamkan pesan ini sebelum orb tersebut berpindah dimensi.',
		'Message on the orb:\nSetelah ayah disegel, aku dan yang lainnya terus dipukul mundur, sampai akhirnya kami bersembunyi di dungeon rahasia milik ayah.',
		'Message on the orb:\nNamun, sekarang, 7 tahun setelah hari itu, aku telah berhasil membunuh pahlawan Ariel dan melepaskan segel pada orb yang dijaganya.',
		'Message on the orb:\nTidak lama lagi aku pasti akan membebaskan ayah.',
	],
	[
		'Message on the orb:\nAyah, sudah 7 tahun 6 bulan sejak hari itu, perang kembali berkecamuk. Kali ini kami bisa bertahan, mungkin aku sudah menjadi sehebat ayah?',
		'Message on the orb:\nKami berhasil menjebak pahlawan Nara dan membunuhnya. Di saat terakhirnya, ia berkata aku sudah menjadi sangat cantik seperti ibu ...',
		'Message on the orb:\nAyah, aku rindu ... Aku ingin kita bisa berkumpul kembali ...'
	],
	[
		'Message on the orb:\nAyah, sudah 9 tahun sejak hari itu, kali ini aku memakan waktu yang cukup lama, para pahlawan yang tersisa telah berkumpul kembali.', 
		'Message on the orb:\nMeskipun banyak dari kami yang gugur, tapi kami berhasil membunuh pahlawan Lucas.',
		'Message on the orb:\nTinggal sedikit lagi, ayah dapat keluar dari tempat itu, dan kita bisa mewujudkan impian ibu, dunia dimana tidak ada lagi penindasan terhadap demi-human dan monster.'
	],
	[
		'Message on the orb:\nAyah, aku sudah lelah. Teman-temanku sudah tiada, Lina, Cheli, dan Kala telah pergi, mungkin selanjutnya giliranku?',
		'Message on the orb:\nRasanya masih kemarin saat kami belajar bersama-sama dan ditegur ibu saat kami tidak mengerti.'
	],
	[	
		'Message on the orb:\nAyah, akhirnya aku berhasil membuka seluruh segelnya, tapi aku tidak yakin apakah kita bisa bertemu lagi ...'
	],
]
var timer_on_stop = false

func reset():
	dark_exposure_time = 0
	exposed_to_dark = false;
	on_cooldown_light_of = false;
	time_until_recover = 0
	get_node("/root/Stage/CanvasLayer/TimeUntilRecoverLabel").text = "Time Until Recover: 0"
	get_node("/root/Stage/Player/Light").enabled = true
	get_node("/root/Stage/Player/Dark").enabled = false
	get_node("/root/Stage/Player/Timer").stop()

func player_spawn():
	Global.reset()

	if Global.die:
		Global.die = false;
		Global.lives -= 1;
	var lives_label = get_node("/root/Stage/CanvasLayer/LivesLabel")
	var orb_count_label = get_node("/root/Stage/CanvasLayer/OrbCountLabel")
	lives_label.text = 'Lives: ' + str(Global.lives) + '/' + str(Global.max_lives)
	orb_count_label.text = 'Orb Collected: ' + str(Global.current_orb) + '/' + str(Global.orbs_limit)
	
