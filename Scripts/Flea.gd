extends Area2D

var startPos
var moveSign = -1

export (int) var moveRightRange = 350
export (int) var moveLeftRange = 350

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(true)
	startPos = position.x

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position.x += moveSign*100*delta
	if position.x <= startPos-moveLeftRange :
		get_node("AnimatedSprite").set_flip_h(true)
		moveSign = 1
	if position.x >= startPos+moveRightRange :
		get_node("AnimatedSprite").set_flip_h(false)
		moveSign = -1

func _on_Flea_body_entered(body):
	var current_scene = get_tree().get_current_scene().get_name()
	if body.get_name() == "Player" && !Global.exposed_to_dark:
		Global.die = true;
		if (Global.lives <= 1 and Global.die):
			get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
		else:
			body.position = Global.player_pos
			Global.player_spawn()
