extends Timer

func _on_TeleportTImer_timeout():
	var instance = Global.teleport_marker_instance
	Global.teleport_marker_instance = null
	get_node("/root/Stage").remove_child(instance)
	get_node("../TeleportTimer").stop()
