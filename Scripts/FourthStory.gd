extends ParallaxLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var dialog_text = get_node("/root/Node2D/DialogBox/RichTextLabel")

# Called when the node enters the scene tree for the first time.
func _ready():
	dialog_text.run_dialog([
		'April 1385 Kalendar Lumina, ditemukan metode untuk menyegel ' +
		'Alastair. Para pahlawan bersiap untuk peperangan terakhir, ' +
		'mempertaruhkan segalanya untuk menyegel Alastair di ' +
		'peperangan tersebut.'], "res://Scenes/FifthStory.tscn")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
