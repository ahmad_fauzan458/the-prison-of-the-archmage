extends ParallaxLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var dialog_text = get_node("/root/Node2D/DialogBox/RichTextLabel")

# Called when the node enters the scene tree for the first time.
func _ready():
	dialog_text.run_dialog([
		'Agustus 1384 Kalendar Lumina, setengah wilayah manusia telah ' +
		'jatuh ke tangan Alastair. Kerajaan manusia Ravenyard menggunakan ' + 
		'kekuatan dari pohon mana untuk memanggil lima pahlawan dari ' +
		'dunia lain.'], "res://Scenes/ThirdStory.tscn")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
