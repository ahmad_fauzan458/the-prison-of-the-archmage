extends RichTextLabel

var dialog = []
var page = 0
var change_scene_target = null;
var player_name = null;

func run_dialog(current_dialog, change_scene = null, name=null):
	change_scene_target = change_scene
	
	player_name = name
	get_node("./../../DialogBox").visible = true
	Global.dialog_on = true;
	dialog = current_dialog;
	set_bbcode(dialog[page])
	if name:
		set_visible_characters(name.length() + 2)
	else:
		set_visible_characters(0)
	
func stop_dialog():
	dialog = []
	page = 0
	Global.dialog_on = false;
	if (change_scene_target):
		get_tree().change_scene(str(change_scene_target))
	else:
		get_node("./../../DialogBox").visible = false

func _process(delta):
	if Global.dialog_on && Input.is_action_just_pressed('space'):
		if get_visible_characters() > get_total_character_count():
			if page < dialog.size() - 1:
				page += 1
				set_bbcode(dialog[page])
				if name:
					set_visible_characters(name.length() + 2)
				else:
					set_visible_characters(0)
			else:
				stop_dialog()
		else:
			set_visible_characters(get_total_character_count())

func _on_Timer_timeout():
	set_visible_characters(get_visible_characters() + 1)
